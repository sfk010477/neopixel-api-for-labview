void twinkle()
{
  uint16_t rand1;
  uint16_t rand2;
  uint8_t delay1;
  uint8_t delay2;
  uint8_t delay3;
  uint16_t delay4;
  rand1 = random16(NUM_LEDS);
  rand2 = random16(NUM_LEDS);
  delay1 = random8(10,200);
  delay2 = random8(10,100);
  delay3 = random8(10,200);
  delay4 = random16(30,2000);
  for(int i = 0; i < NUM_LEDS; i++) 
  {
    value = 40;//map(i, 0, (NUM_LEDS-1), 0, 255);
    hsv.v = value;
    hsv2rgb_rainbow( hsv, rgb1);
    leds[i] = rgb1; 
  }
  leds[rand1]=CRGB::White;
  FastLED.show();
  delay(delay1);
  leds[rand2]=CRGB::White;
  FastLED.show();
  delay(delay2);
  leds[rand1]=rgb1;
  FastLED.show();
  delay(delay3);
  leds[rand2]=rgb1;
  FastLED.show();
  delay(delay4);
}
