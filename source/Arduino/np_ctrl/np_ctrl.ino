// ================================================================================
// UDP Test
// This sketch receives UDP message strings via multicast and prints them 
// to the serial port. This code is based on Michael Margolis' public domain UDP example. 

#include <SPI.h>          // needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>  // UDP library from: bjoern@cs.stanford.edu 12/30/2008

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress local_ip(192,168,2,200);
IPAddress dns(192,168,2,1);
IPAddress gateway(192,168,2,1);
IPAddress subnet(255,255,255,0);

unsigned int localPort = 58432;             // local port to listen on

// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet,

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;


// ================================================================================


// ********************************************************************************
#include <FastLED.h>

#if defined(FASTLED_VERSION) && (FASTLED_VERSION < 3001000)
#warning "Requires FastLED 3.1 or later; check github for latest code."
#endif

// How many leds in your strip?
#define NUM_LEDS 300

// For led chips like Neopixels, which have a data line, ground, and power, you just
// need to define DATA_PIN.  For led chipsets that are SPI based (four wires - data, clock,
// ground, and power), like the LPD8806, define both DATA_PIN and CLOCK_PIN
#define DATA_PIN 0
#define ENABLE_LEVELSHIFT 6

// define special LED positions, e.g. corners of the rack
#define Begin1  10
#define End1    110
#define End2    190
#define Begin2  290


// Controller-specific settings
#define LED_TYPE   WS2812
#define COLOR_ORDER   GRB

// Define the array of leds
CRGB leds[NUM_LEDS];

// Default values for color
static uint8_t r = 77;
static uint8_t g = 33;
static uint8_t b = 122;//ok

// ********************************************************************************
  CHSV hsv(160, 255, 255); // pure blue in HSV Rainbow space
  CRGB rgb(r, g, b);
  CRGB rgb1(0, 153, 153);
  uint8_t hue;
  uint8_t value;
  uint16_t m;
// ********************************************************************************


void setup() {
  delay( 3000 ); //safety startup delay

  // ================================================================================
  // start the Ethernet and UDP:
  Ethernet.begin(mac,local_ip,dns,gateway,subnet);
  Udp.begin(localPort);

  Serial.begin(9600);
  Serial.println("starting...");

  // ================================================================================
  
  // ********************************************************************************
  //enable levelshifter on connector pcb
  pinMode(ENABLE_LEVELSHIFT, OUTPUT);
  digitalWrite(ENABLE_LEVELSHIFT, HIGH);

  LEDS.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds,NUM_LEDS).setCorrection(TypicalLEDStrip);
  LEDS.setBrightness(255);
  FastLED.setMaxPowerInVoltsAndMilliamps(5,10000); 
  // ********************************************************************************
}

void loop()
{
  //progress();
  //twinkle();
  rainbowwheel();
  //knightrider();
  
}


/*
uint8_t scene = 0;

void loop()
{
  scene = getSceneFromETH(); //returns an enum(u8) enumerating the the scene/function called by LabVIEW
  switch (scene){
    case 0:
    idle();
    break;
    case 1:
    progress();
    break;
    case 2:
    twinkle();
    break;
    case 3
    rainbowwheel();
    break;
    //case ...
    //future scenes
    default:
    //make some special error visualization
    brake;

    //delays, extracting additional parameters from ETH buffer etc. must be handled within the corresponding scene
    //scene cannot change during executing one iteration of the current scene. however, if implemented the scene itself can check Ethernet of parameter updates during execution
 
  }
}

uint8_t getSceneFromETH(){
return sceneenumerator;
*/
